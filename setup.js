const prompt = require('prompt');
const fs = require('fs');

prompt.get(['ip', 'mac', 'appKey', 'secretKey', 'deviceId'], function (err, result) {
    console.log('Command-line input received:');
    console.log('  ip: ' + result.ip);
    console.log('  mac: ' + result.mac);
    console.log('  appKey: ' + result.appKey);
    console.log('  secretKey: ' + result.secretKey);
    console.log('  deviceId: ' + result.deviceId);
    const credentials = {
        ip: result.ip,
        mac: result.mac,
        appKey: result.appKey,
        secretKey: result.secretKey,
        deviceId: result.deviceId
    }
    fs.writeFileSync('./credentials.json', JSON.stringify(credentials));
});