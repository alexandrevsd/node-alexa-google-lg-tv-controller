const {Tv} = require('node-lgtv-controller');
const {SinricPro, SinricProActions} = require("sinricpro");

const {ip, mac, appKey, secretKey, deviceId} = require('./credentials.json');
const devicesId = [deviceId];

const tv = new Tv(ip, mac);
const sinricSocket = new SinricPro(appKey, devicesId, secretKey, true);

async function setPowerState(deviceId, data) {
    if(data === 'Off') {
        await tv.turnOff();
    } else if(data === 'On') {
        await tv.turnOn();
    }
    return true;
}

async function setVolume(deviceId, volume) {
    await tv.setVolume(volume);
    return true;
}

async function adjustVolume(deviceId, volume) {
    return await setVolume(volume);
}

async function setMute(deviceId, data) {
    const muted = !!data;
    await tv.setMute(muted)
    return true;
}

async function selectInput(deviceId, data) {
    const inputName = data.replace(' ', '_');
    await tv.setInput(inputName);
    return true;
}

const callbacks = {
    setPowerState,
    setVolume,
    adjustVolume,
    setMute,
    selectInput
};

SinricProActions(sinricSocket, callbacks);